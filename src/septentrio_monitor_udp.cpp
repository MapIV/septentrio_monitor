#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ros/ros.h"
#include "diagnostic_updater/diagnostic_updater.h"

#include "sbf_conv.hpp"

#define BUFFER_SAFE 2000
#define BUFFER_SIZE 2048

#define ERROR_CPU_LOAD 100
#define WARN_CPU_LOAD 95

struct sockaddr_in dstAddr;
static bool is_connected = false;
static double rate = 10; //Hz
static double counter_threshold = rate * 3; //3sec
static ReceiverStatus receiver_status;

static diagnostic_updater::Updater* p_updater;

static void check_software_error(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if ((receiver_status.data.rxerror >> 3) & 0x01 || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_watchdog_error(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if ((receiver_status.data.rxerror >> 4) & 0x01 || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_antenna_error(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if ((receiver_status.data.rxerror >> 5) & 0x01 || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_congestion_error(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if ((receiver_status.data.rxerror >> 6) & 0x01 || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_missedevent_error(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if ((receiver_status.data.rxerror >> 8) & 0x01 || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_invalidconfig_error(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if ((receiver_status.data.rxerror >> 10) & 0x01 || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_outofgeofence_error(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if ((receiver_status.data.rxerror >> 11) & 0x01 || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_antenna_state(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if (!((receiver_status.data.rxstate >> 1) & 0x01) || is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void check_cpu_load_state(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if (receiver_status.data.cpuload >= WARN_CPU_LOAD && receiver_status.data.cpuload < ERROR_CPU_LOAD) {
    level = diagnostic_msgs::DiagnosticStatus::WARN;
    msg = "Warning";
  }
/*
  else if (receiver_status.data.cpuload >= ERROR_CPU_LOAD || is_connected == false) {
    if (is_connected == false){
      receiver_status.data.cpuload = 0.0;
    }
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }
*/

  stat.addf("CPU Load [%]", "%d", receiver_status.data.cpuload);
  stat.summary(level, msg);
}

static void check_connection_state(diagnostic_updater::DiagnosticStatusWrapper& stat) {
  uint8_t level = diagnostic_msgs::DiagnosticStatus::OK;
  std::string msg = "OK";

  if (is_connected == false) {
    level = diagnostic_msgs::DiagnosticStatus::ERROR;
    msg = "Error";
  }

  stat.summary(level, msg);
}

static void packet_receive_rate(int fd, double rate) {
  ros::Rate loop_rate(rate);
  int connection_status_counter = 0;
  while (ros::ok()) {
    
    int rem;
    int ret;
    bool chksum;
    char buffer[BUFFER_SIZE];
    char* w_buffer = buffer;
    char* r_buffer = buffer;
    char* buffer_end = &buffer[sizeof(buffer)];
    bool header_enable = false;

    
    errno = 0;
    ret = recv(fd, w_buffer, buffer_end - w_buffer - 1, 0);
    if (ret > 0) {
      is_connected = true;
      w_buffer += ret;
      connection_status_counter = 0;
      //ROS_INFO("septentrio recived status data");
    }
    else if (ret == 0) {
      close(fd);
      return;
    }
    else {
      if (errno == EAGAIN) {
        loop_rate.sleep();
        connection_status_counter ++;
        if (connection_status_counter < counter_threshold){
          is_connected = false;
          continue;
        }
        ROS_ERROR("septentrio not recive status data");
      }
      else {
        ros::shutdown();
      }
    }

    int buffer_num = (w_buffer - 1) - r_buffer;
    while ((w_buffer - 1) - r_buffer > 8) {
      char* sync1 = strchr(r_buffer, '$');
      if ((sync1 != NULL) && ( *(sync1+1) == '@') ) {
        const BlockHeader_t * block = (const BlockHeader_t *) sync1;
        uint16_t id = block->ID & 0x01FFF;
        r_buffer = sync1;
        r_buffer += block->Length;
        header_enable = true;

        if (isValid(sync1)) {
          header_enable = false;
          if (id == 4014) {
            receiver_status = decodeReceiverStatus(sync1);
          }
        }
      } else {
        break;
      }
    }

    rem = w_buffer - r_buffer;

    if (rem > BUFFER_SAFE) {
      ROS_WARN("Buffer over. Init Buffer.");
      rem = 0;
      ret = 0;
      w_buffer = buffer;
      r_buffer = buffer;
      memset(buffer, 0x00, BUFFER_SIZE);
    }
    else if (header_enable) {
      ROS_INFO("wait data part.");
      memmove(buffer, r_buffer, rem);
      w_buffer = buffer + rem;
    }
    else if (rem == 0) {
      ret = 0;
      w_buffer = buffer;
      r_buffer = buffer;
      memset(buffer, 0x00, BUFFER_SIZE);
    }
    p_updater->force_update();
    ros::spinOnce();
    
  }
  close(fd);
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "septentrio_monitor_udp");
  ros::NodeHandle node_handle_;

  diagnostic_updater::Updater updater;
  p_updater = &updater;

  updater.setHardwareID("septentrio");
  updater.add("gnss_software", check_software_error);
  updater.add("gnss_watchdog", check_watchdog_error);
  updater.add("gnss_antenna_error", check_antenna_error);
  updater.add("gnss_congestion", check_congestion_error);
  updater.add("gnss_missed_event", check_missedevent_error);
  updater.add("gnss_invalid_config", check_invalidconfig_error);
  updater.add("gnss_out_of_geofence", check_outofgeofence_error);
  updater.add("gnss_antenna_state", check_antenna_state);
  updater.add("gnss_cpu_load", check_cpu_load_state);
  updater.add("gnss_connection", check_connection_state);

  int port,sock;
  std::string address;
  int result, val;


  // Read parameters
  node_handle_.param("septentrio_monitor_udp/address", address, std::string("127.0.0.1"));
  node_handle_.param("septentrio_monitor_udp/port", port, 62001);

  sock = socket(AF_INET, SOCK_DGRAM, 0);

  memset(&dstAddr, 0, sizeof(dstAddr));
  dstAddr.sin_family = AF_INET;
  dstAddr.sin_addr.s_addr = INADDR_ANY;
  dstAddr.sin_port = htons(port);

  ros::Rate loop_rate(1.0);
  while (ros::ok())
  {
    result = bind(sock, (struct sockaddr *)&dstAddr, sizeof(dstAddr));
    if( result < 0 )
    {
      /* non-connect */
      ROS_WARN_THROTTLE(1, "ip:%s port:%d connect try...", address.c_str(), port);
    }
    else
    {
      /* connect */
      ROS_INFO("ip:%s port:%d connected", address.c_str(), port);
      break;
    }
    ros::spinOnce();
    loop_rate.sleep();
  }

  if (ros::ok())
  {
    /* non-block *//* ros::rate() */
    val = 1;
    ioctl(sock, FIONBIO, &val);
    packet_receive_rate(sock, rate);
  }

  return 0;
}
